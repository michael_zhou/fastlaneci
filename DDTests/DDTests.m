//
//  DDTests.m
//  DDTests
//
//  Created by 周琪樺 on 17/9/2019.
//  Copyright © 2019 Michael. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "PP.h"
@interface DDTests : XCTestCase

@end

@implementation DDTests

- (void)setUp {
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
}

- (void)testExample {
    // This is an example of a functional test case.
    // Use XCTAssert and related functions to verify your tests produce the correct results.
    
    int result = [PP number];
    XCTAssertEqual(result, 10,@"");
    
    int result2 = [PP number2];
    XCTAssertEqual(result2, 20,@"");

}

- (void)testPerformanceExample {
    // This is an example of a performance test case.
    [self measureBlock:^{
        // Put the code you want to measure the time of here.
    }];
}

@end
