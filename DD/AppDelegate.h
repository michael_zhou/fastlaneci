//
//  AppDelegate.h
//  DD
//
//  Created by 周琪樺 on 17/9/2019.
//  Copyright © 2019 Michael. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

